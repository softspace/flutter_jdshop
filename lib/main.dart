import 'package:flutter/material.dart';

import 'package:flutter_jdshop/routers/router.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(750,1334),
      builder:()=> MaterialApp(
        theme: ThemeData(

        ),
        onGenerateRoute: onGenerateRoute,
        //配置初始化路由
        initialRoute: '/',
      ),
    );
  }
}