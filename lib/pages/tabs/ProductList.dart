import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ProductListPage extends StatefulWidget {
  Map arguments;

  ProductListPage({Key? key, required this.arguments}) : super(key: key);

  @override
  _ProductListPageState createState() => _ProductListPageState();
}

class _ProductListPageState extends State<ProductListPage> {


  //实现自定义时间打开侧边栏
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  //获取商品列表



  //商品列表
  Widget _productListWidget() {
    return Padding(
      padding: EdgeInsets.fromLTRB(10, 80.h, 10, 10),
      child: ListView.builder(
          itemCount: 10,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Divider(),
                Row(
                  children: [
                    Container(
                      width: 180.r,
                      height: 180.r,
                      child: Image.network(
                        'https://www.itying.com/images/flutter/list2.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 180.r,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '戴尔(DELL)灵越3670 英特尔酷睿i5 高性能 台式电脑整机(九代i5-9400 8G 256G)',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Row(
                              children: [
                                Container(
                                  height: 36.h,
                                  margin: EdgeInsets.only(right: 10),
                                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Color.fromRGBO(230, 230, 230, 0.9),
                                  ),
                                  child: Text('4g'),
                                ),
                              ],
                            ),
                            Text(
                              '￥999',
                              style: TextStyle(color: Colors.red, fontSize: 16),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            );
          }),
    );
  }


  //筛选导航
  Widget _subHeaderWidget(){
    return  Positioned(
        top: 0,
        height: 80.h,
        width: 750.w,
        child: Container(
          height: 80.h,
          width: 750.w,
          // color: Colors.red,
          child: Row(children: [
            Expanded(
              flex: 1,
              child: InkWell(
                  onTap: () {},
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20.h, 0,20.h ),
                    child: Text(
                      '综合',
                      textAlign: TextAlign.center, style: TextStyle(color: Colors.red),
                    ),
                  )),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                  onTap: () {},
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20.h, 0,20.h ),
                    child: Text(
                      '销量',
                      textAlign: TextAlign.center,
                    ),
                  )),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                  onTap: () {},
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20.h, 0,20.h ),
                    child: Text(
                      '价格',
                      textAlign: TextAlign.center,
                    ),
                  )),
            ),
            Expanded(
              flex: 1,
              child: InkWell(
                  onTap: () {this._scaffoldKey.currentState!.openEndDrawer();},
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20.h, 0,20.h ),
                    child: Text(
                      '筛选',
                      textAlign: TextAlign.center,
                    ),
                  )),
            ),
          ]),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,

      endDrawer: Drawer(child: Container(child: Text(
        '实现筛选'
      ),),),
          appBar: AppBar(title: Text('商品列表'),
          actions: [Container()],),
        body: Stack(
          children: [
            _productListWidget(),
            _subHeaderWidget(),
          ],
        ));
  }
}
