import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jdshop/config/Config.dart';
import 'package:flutter_jdshop/model/CateModel.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class CategoryPage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage>
    with AutomaticKeepAliveClientMixin {
  int _selectIndex = 0;

  List<CateItemModel> leftCateList = [];
  List<CateItemModel> rightCateList = [];

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  Widget _leftCateWidget(leftWidth) {
    return Container(
      height: double.infinity,
      // color: Colors.red,
      width: leftWidth,
      child: ListView.builder(
          itemCount: this.leftCateList.length,
          itemBuilder: (context, index) {
            return Column(
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      _selectIndex = index;
                      this._getRightCateData(this.leftCateList[index].sId);
                    });
                  },
                  child: Container(
                    child: Text(
                      '${this.leftCateList[index].title}',
                      textAlign: TextAlign.center,
                    ),
                    color: _selectIndex == index
                        ? Color.fromRGBO(240, 246, 246, 0.9)
                        : Colors.white,
                    padding: EdgeInsets.only(top: 24.h),
                    height: 84.h,
                    width: double.infinity,
                  ),
                ),
                Divider(height: 1)
              ],
            );
          }),
    );
  }

  Widget _rightCateWidget(rightItemWidth, rightItemHeight) {
    return this.rightCateList.length == 0
        ? Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(10),
              height: double.infinity,
              color: Color.fromRGBO(240, 246, 246, 0.9),
              child: Text("加载中..."),
            ))
        : Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.all(20.w),
              height: double.infinity,
              child: GridView.builder(
                itemBuilder: (context, index) {
                  String pic = this.rightCateList[index].pic!;
                  pic = Config.domain + pic.replaceAll('\\', '/');
                  return InkWell(
                    onTap: (){
                      Navigator.pushNamed(context, '/productList',arguments: {
                        'cid':this.rightCateList[index].sId
                      });

                    },
                    child: Container(
                      height: rightItemHeight,
                      width: rightItemWidth,
                      child: Column(
                        children: [
                          AspectRatio(
                            aspectRatio: 1 / 1,
                            child: Image.network(pic, fit: BoxFit.cover),
                          ),
                          Container(
                              height: 32.sp,
                              child: Text(
                                '${this.rightCateList[index].title}',
                              )),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: this.rightCateList.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, //横轴三个子widget
                  childAspectRatio: 1 / 1.2, //宽高比为1时，子widget
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                ),
              ),
              color: Color.fromRGBO(240, 246, 246, 0.9),
            ),
          );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getLeftCateData();
  }

  //请求左侧分类的数据
  _getLeftCateData() async {
    var api = '${Config.domain}api/pcate';
    Response result = await Dio().get(api);
    var leftCateList = CateModel.fromJson(result.data);

    setState(() {
      this.leftCateList = leftCateList.result!;
    });

    _getRightCateData(this.leftCateList[this._selectIndex].sId);
  }

  //请求右侧分类的数据
  _getRightCateData(pid) async {
    var api = '${Config.domain}api/pcate';
    Response result = await Dio().get(api, queryParameters: {"pid": pid});
    var rightCateList = CateModel.fromJson(result.data);
    setState(() {
      this.rightCateList = rightCateList.result!;
    });
  }

  @override
  Widget build(BuildContext context) {
    var leftWidth = 1.sw / 4;
    var rightItemWidth = (1.sw - leftWidth) / 3 - 10 - 20.w * 2;

    //获取计算后的宽度
    rightItemWidth = rightItemWidth.w;

    //获取计算后的高度
    var rightItemHeight = rightItemWidth + 32.h;

    return Row(
      children: [
        _leftCateWidget(leftWidth),
        _rightCateWidget(rightItemWidth, rightItemHeight),
      ],
    );
  }
}
