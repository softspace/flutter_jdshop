import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_jdshop/config/Config.dart';
import 'package:flutter_jdshop/model/FocusModel.dart';
import 'package:flutter_jdshop/model/ProductModel.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper_null_safety/flutter_swiper_null_safety.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AutomaticKeepAliveClientMixin{
  List<Result> _focusDate = [];
  List<ProductModelResult> _hotProductList = [];
  List<ProductModelResult> _bestProductList = [];

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getFocusData();
    _getHotProductData();
    _getBestProductData();
  }

  //获取轮播图数据
  _getFocusData() async {
    var result = await Dio().get(Config.domain+'api/focus');
    var focusList = FocusModel.fromJson(result.data).result;
    setState(() {
      this._focusDate = focusList!;
    });
  }

  //获取猜你喜欢的数据
  _getHotProductData() async {
    var api = '${Config.domain}api/plist';
    Response response = await Dio().get(api, queryParameters: {"is_hot": 1});
    var hotProductList = ProductModel.fromJson(response.data);
    setState(() {
      this._hotProductList = hotProductList.result!;
    });
  }

  //获取热门推荐的数据
  _getBestProductData() async {
    var api = '${Config.domain}api/plist';
    Response response = await Dio().get(api, queryParameters: {"is_best": 1});
    var bestProductList = ProductModel.fromJson(response.data);
    setState(() {
      this._bestProductList = bestProductList.result!;
    });
  }

  //轮播图
  Widget _swiperWidget() {

    if (this._focusDate.length > 0) {
      return Container(
        //配置宽高比
        child: AspectRatio(
          aspectRatio: 2 / 1,
          child: Swiper(
            itemBuilder: (BuildContext context, int index) {
              return new Image.network(
                'http://jd.itying.com/${this._focusDate[index].pic!.replaceAll('\\', '/')}',
                fit: BoxFit.fill,
              );
            },
            // itemCount: this._focusDate.length,
            itemCount: this._focusDate.length,
            pagination: new SwiperPagination(),
            //自动轮播
            autoplay: true,
          ),
        ),
      );
    } else {
      return Text('加载中');
    }
  }

  Widget _titleWidget(String value) {
    return Container(
      height: 32.h,
      //内边距
      margin: EdgeInsets.only(left: 20.w),
      padding: EdgeInsets.only(left: 20.w),
      decoration: BoxDecoration(
          //边框
          border: Border(
              left: BorderSide(
        color: Colors.red,
        width: 4,
      ))),
      child:
          Text(value, style: TextStyle(color: Colors.black54, fontSize: 26.sp)),
    );
  }

  Widget _hotProductListWidget() {
    if (this._hotProductList.length > 0) {
      return Container(
        height: 200.h,
        padding: EdgeInsets.fromLTRB(20.r, 10.r, 10.r, 0.r),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            String sPic = this._hotProductList[index].sPic!;
            sPic = Config.domain + sPic.replaceAll('\\', '/');
            return Column(
              children: [
                Container(
                    height: 140.r,
                    width: 140.r,
                    margin: EdgeInsets.only(right: 10.w),
                    child: Image.network(
                      // 'https://www.itying.com/images/flutter/hot${index + 1}.jpg',
                      sPic,
                      fit: BoxFit.cover,
                    )),
                Container(
                  padding: EdgeInsets.only(top: 10.h),
                  height: 44.h,
                  child: Text(
                    '${this._hotProductList[index].price}',
                    style: TextStyle(fontSize: 26.sp, color: Colors.red),
                  ),
                )
              ],
            );
          },
          itemCount: this._hotProductList.length,
        ),
      );
    } else {
      return Text('加载中');
    }
  }

  //推荐商品
  Widget _recProductListWidget() {
    var itemWidth = (1.sw - 30) / 2;

    return Container(
      padding: EdgeInsets.all(10),
      child: Wrap(
        //间距
        runSpacing: 10,
        //运行间距
        spacing: 10,
        children: this._bestProductList.map((value) {
          String sPic = value.sPic!;
          sPic = Config.domain + sPic.replaceAll('\\', '/');

          return Container(
            width: itemWidth,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                border: Border.all(
              color: Colors.black12,
              width: 1,
            )),
            child: Column(
              children: [
                Container(
                    width: double.infinity,
                    child: AspectRatio(
                      aspectRatio: 1 / 1, //防止服务器返回的图片大小不一致
                      child: Image.network(
                        sPic,
                        fit: BoxFit.cover,
                      ),
                    )),
                Padding(
                  padding: EdgeInsets.only(top: 10.h),
                  child: Text(
                    value.title!,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      color: Colors.black54,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10.h),
                  child: Stack(
                    children: [
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            '￥${value.price}',
                            style:
                                TextStyle(color: Colors.red, fontSize: 36.sp),
                          )),
                      Align(
                          alignment: Alignment.centerRight,
                          child: Text(
                            '￥${value.oldPrice}',
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 28.sp,
                                decoration: TextDecoration.lineThrough),
                          )),
                    ],
                  ),
                )
              ],
            ),
          );
        }).toList(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        _swiperWidget(),
        SizedBox(height: 20.h),
        _titleWidget('猜你喜欢'),
        SizedBox(height: 20.h),
        _hotProductListWidget(),
        _titleWidget('热门推荐'),
        _recProductListWidget(),
      ],
    );
  }


}
